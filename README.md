# Dockerized Nextcloud for mid-scale systems

Dockerized Nextcloud with everything.

- Netxcloud
- Push
- MariaDB
- OnlyOffice (without mobile limitations)
- Redis

## Getting started

**Clone with submidules!**

```shell
git clone --recurse-submodules https://gitlab.com/activism.international/nextcloud-docker.git

cp db.env.example db.env

# adjust env file

# please review the dockerfiles in `build/`

docker-compose build

docker-compose up -d
```

## Backup server data (not user data)

```bash
# get the app data dir first
export APP_DATA_DIR=$(sudo ls data/data | grep appdata)

# now rsync all files except of the user data to a backup directory
sudo rsync -av --progress data/{app,data/$APP_DATA_DIR,fonts/mariadb,onlyoffice,redis,skeletron} --exclude $APP_DATA_DIR/preview ../nextcloud-backup
```

## Upgrade nextcloud

**Ensure you have a backup.**

```bash

sudo cp -ar data/skeleton skeleton-save

docker pull nextcloud:alpine
docker-compose build --no-cache # with cache it unfortunately doesn't upgrade
docker-compose up -d
docker-compose exec -u www-data app php occ upgrade
```

Now, you usually get an infinite loop asking you to upgrade some of the built-in nextcloud apps. Please perform this update and manually copy the `custom_apps/WHATEVER` into your `data/app/custom_apps`.

Sometimes, it also works to simply remove (backup first) the corresponding app folders (e.g. data/app/custom_apps/MYAPP) and to re-download after the update using the normal nextcloud app store.

In case nothing works, you may still manually re-enable broken apps in the mariadb table `oc_appconfig`. Please don't forget to update the stated version installed there.

Commonly, the final solution is a trial-and-error game using all the three methods. Good luck.

```bash
sudo rm -rf data/skeleton/*
sudo cp -ar skeleton-save/* data/skeleton
sudo rmdir skeleton-save
```
